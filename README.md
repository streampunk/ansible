streampunk.cc Ansible config
===

This playbook will install
[autoradio](https://git.autistici.org/ale/autoradio) on a cluster of
machines, along with the necessary infrastructure (etcd, logging,
monitoring and alerting).

# Configuration

There are some mandatory configuration variables that need to be set
in *group_vars/all/vars.yml* (which ships with defaults that are only
appropriate for the Vagrant test environment). They are:

* *public_domain* - the DNS domain to use to host the streaming
  infrastructure
* *admin_emails* - a list of email addresses that will receive alerts
  and other administrative spam
* *smtp_server* - SMTP server to use for outbound email
* *smtp_auth_user* - username for SMTP authentication
* *smtp_auth_password* - password for SMTP authentication

## Networking

The playbook supports split networking scenarios, where the IP address
used by the public is different than the one used by peers to
communicate among themselves.

For every host, the Ansible variables *public_ip* and *peer_ip* must
be defined. There are two ways to do this:

* you can specify these values explicitly in the Ansible inventory for
  each host, or
* Ansible can auto-detect the IP addresses if you specify the network
  interfaces used for public/peer networking with the
  *public_network_interface* and *internal_network_interface*
  variables.

By default, *public_network_interface* and
*internal_network_interface* are both set to `ens6`, which happens to
work well with the test Vagrant setup.

## Inventory Groups

If you are using the default *site.yml* playbook, there are some host
groups that will need to be defined in the Ansible inventory:

* *etcd* - hosts that will be running etcd
* *monitor* - hosts that will be running the monitoring stack

You should have at least 3 hosts in the *etcd* group, and at least 2
in the *monitor* group (for redundancy).

# Testing

This repository contains a script to build dynamic, ephemeral test
environments using virtual machines, so that different combinations of
Ansible / autoradio / other packages can be tested before deploying
them to production.

```shell
$ test.sh my-test
```

The above command will generate an Ansible configuration in the
*my-test* directory, it will start some VMs, and it will run Ansible
on them deploying the configuration from this repository.

The test environment can be customized by setting environment
variables when calling test.sh:

* `DIST`: select the Debian distribution to use (default "buster")
* `LIBVIRT_USER`, `LIBVIRT_HOST`: use libvirt-over-SSH, needs SSH
passwordless authentication to be set up.
* `APT_PROXY`: set to the *host:port* address of apt-cacher-ng. When
using this directive, ensure that the apt-cacher-ng configuration has
a permissive PassThroughPattern to allow access to https://
repositories.
* `VM`: select the VM manager to use, one of "vagrant" (default) or
"vmine" (used in CI setups).
* `VERBOSE`: set to a non-empty value to run ansible-playbook with
increased verbosity.
* `AUTORADIO_SRC`: if set, points at a local copy of the autoradio
source repository which will be compiled and installed instead of
using the default packages from the autoradio Debian repository.

So, for example, if you have a copy of the *autoradio* source
repository with some local changes that you want to test, you can use:

```shell
$ AUTORADIO_SRC=/path/to/git.autistici.org/ale/autoradio test.sh my-test
```

and the test environment in *my-test* will be set up to build
autoradio from your local sources.

Check the *hosts.ini* file in the my-test directory to find out the IP
addresses of the virtual machines (which are randomly generated).

## Test stream

The *test-source.yml* Ansible playbook can be used to quickly setup a
stream and an associated source for testing purposes. Use the
*stream_name* Ansible variable to set the name of the stream to be
created:

```shell
$ ansible-playbook -i hosts.ini \
    -e stream_name=/test.ogg \
    test-source.yml
```

Then it's easy to create test loads with, e.g., autoradio's
*radiobench* utility:

```shell
$ go run git.autistici.org/ale/autoradio/cmd/radiobench \
    -n 100 http://10.236.82.10/test.ogg
```

will start 100 listeners on the stream just created above.
