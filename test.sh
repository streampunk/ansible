#!/bin/bash
#
# Test driver for the streampunk Ansible configuration (and associated
# software).
#
# The tool will generate a test environment using ephemeral virtual
# machines, and run Ansible on it using the test configuration
# parameters in group_vars.
#
# Configuration is entirely controlled via environment variables:
#
# - DIST: select the Debian distribution to use (default "bullseye")
#
# - LIBVIRT_USER, LIBVIRT_HOST: use libvirt-over-SSH, needs SSH
# passwordless authentication to be set up.
#
# - APT_PROXY: set to a host:port address of apt-cacher-ng. When using
# this directive, ensure that the apt-cacher-ng configuration has a
# permissive PassThroughPattern to allow access to https://
# repositories.
#
# - VM: select the VM manager to use, one of "vagrant" (default) or
# "vmine".
#
# - VERBOSE: set to a non-empty value to run ansible-playbook with
# increased verbosity.
#
# - AUTORADIO_SRC: if set, points at a local copy of the autoradio
# source repository which will be compiled and installed instead of
# using the default packages from the autoradio Debian repository.
#

repo_root=$(dirname $(realpath "$0"))
target="${1:-.}"

die() {
    echo "ERROR: $*" >&2
    exit 1
}

wait_for_vms() {
    # Wait at most 30 seconds for the vms to become reachable.
    local i=0
    local ok=1
    while [ $i -lt 10 ]; do
        sleep 1
        if ansible -i hosts.ini all -m ping; then
            ok=0
            break
        fi
        i=$(($i + 1))
    done
    return $ok
}

vm_manager() {
    local what="$1"
    case "${VM:-vagrant}" in
        vagrant)
            vagrant ${what}
            ;;
        vmine)
            local args=
            if [ "${what}" = up ]; then
                args="--image=${dist} host1=${ip_net_prefix}.10 host2=${ip_net_prefix}.11 host3=${ip_net_prefix}.12"
            fi
            ${repo_root}/ci/vmine.py ${libvirt_userhost:+--ssh ${libvirt_userhost}} ${what} ${args}
            ;;
        *)
            die "Unsupported VM manager"
            ;;
    esac
}

set -eu

# Select the Debian distribution to use.
dist="${DIST:-bullseye}"

# Choose a random network in the 10.x range.
ip_net_prefix="10.$(( $RANDOM % 255 )).$(( $RANDOM % 255 ))"

# Find the path for Mitogen.
mitogen_path="$(python3 -c 'import ansible_mitogen;print(ansible_mitogen.__path__[0])')"
[ -n "${mitogen_path}" ] || die "Mitogen not found. Please run 'pip3 install mitogen'"

# If LIBVIRT_* parameters are specified, set up SSH to go through the proxy host.
libvirt_userhost=
ssh_opts=
if [ -n "${LIBVIRT_HOST:-}" ]; then
    libvirt_userhost="${LIBVIRT_USER:-}${LIBVIRT_USER:+@}${LIBVIRT_HOST}"
    ssh_opts="-o ProxyJump=${libvirt_userhost}"
fi

# Render the .in templates.
mkdir -p ${target}
for file in ansible.cfg Vagrantfile hosts.ini ; do
    sed -e "s,@IP_NET@,${ip_net_prefix},g" \
        -e "s,@MITOGEN_PATH@,${mitogen_path},g" \
        -e "s,@SSH_OPTS@,${ssh_opts},g" \
        -e "s,@DIST@,${dist},g" \
        -e "s,@REPO_ROOT@,${repo_root},g" \
        -e "s,@APT_PROXY@,${APT_PROXY:-},g" \
        <${repo_root}/${file}.in >${target}/${file}
done
cp ${repo_root}/site.yml ${target}/site.yml

# Environment is ready, start up the VMs and wait for them to be ready.
(cd ${target} && vm_manager up && wait_for_vms)

# Run Ansible.
(cd ${target} &&
     ansible-playbook -i hosts.ini \
                      ${VERBOSE:+-vv} \
                      ${AUTORADIO_SRC:+-e source_repository_path=${AUTORADIO_SRC}} \
                      site.yml)
