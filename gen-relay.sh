#!/bin/bash

cat <<EOF
#!/usr/bin/liquidsoap

set("log.stdout", true);
set("log.file", false);
set("log.level", 3);

EOF


for mountpoint in "$@"; do
    echo "output.icecast("
    case $mountpoint in
    *.ogg)
      echo "  %vorbis(quality=0.3, samplerate=44100, channels=2),"
      ;;
    *.mp3)
      echo "  %mp3(samplerate=44100, channels=2),"
      ;;
    esac
    cat <<EOF
  mount="${mountpoint}",
  host="localhost",
  port=80,
  user="$(radioctl show-mount $mountpoint | awk -F= '$1 == "username" {print $2}')",
  password="$(radioctl show-mount $mountpoint | awk -F= '$1 == "password" {print $2}')",
  name="$(sed -e 's,/\(.*\)\.[a-z]*$,\1,' <<<$mountpoint)",
  mksafe(input.http("http://s.streampunk.cc${mountpoint}"))
);
EOF
done


